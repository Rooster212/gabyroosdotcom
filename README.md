## View the site at [gabyroos.com](http://gabyroos.com)

[![Build Status](https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg)](https://gitlab.com/%{project_path}/pipelines)

## Build
To install dependencies:

```bash
npm install
```

To run using on `localhost:8080`:
```bash
npm run serve
```

To run in production, I use a custom NGINX container to terminate SSL and then upstream proxy to the container running in Docker.

## Docker

Build using:
```bash
docker build -t gabyroosdotcom .
```

Run using:
```bash
docker run -d -p 8080:8080 gabyroosdotcom
```

## Testing local NGINX (after building container)

Create test network if it doesn't exist:
```bash
docker network create nginx_test
```

Run NGINX with:
```bash
docker run -d --name nginx --network nginx_test -v test_nginx.conf:/etc/nginx/conf.d/gabyroos.conf -p 8080:8080 nginx:alpine
```

Run site with:
```bash
docker run -d --network nginx_test --name gabyroosdotcom gabyroosdotcom
```

## References/Links

[Google Fonts](https://fonts.google.com)

[CSS - A guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[Reactjs - State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)

### Various Gulp resources
[Beginners guide to Gulp](https://andy-carter.com/blog/a-beginners-guide-to-the-task-runner-gulp)

[Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/)

[Heavy Gulp help from Microsoft's TypeScriptSamples repo](https://github.com/Microsoft/TypeScriptSamples/tree/master/react-flux-babel-karma)

[Article: `ES6 + TypeScript + Babel + React + Flux + Karma: The Secret Recipe`](https://blog.johnnyreilly.com/2015/12/es6-typescript-babel-react-flux-karma.html)

## Libraries/Packages

[React Player](https://www.npmjs.com/package/react-player) - For video playback
FROM node:10.9.0-alpine

# Create app directory
WORKDIR /usr/src/gabyroosdotcom

COPY package*.json /usr/src/gabyroosdotcom/
RUN npm install

COPY . /usr/src/gabyroosdotcom/

# Expose our running port
EXPOSE 8080

# Remove un-needed files
RUN rm .eslintrc

# npm start for our default entrypoint
CMD [ "npm", "run", "serve" ]

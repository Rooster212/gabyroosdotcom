import React from 'react';

export default class Social extends React.Component<any, any> {
    render()
    {
        return (
            <div className="social">
                <a className="fa fa-vimeo-square fa-3x" target="_blank" href="https://vimeo.com/user55162378"></a>
                <a className="fa fa-instagram fa-3x" target="_blank" href="https://www.instagram.com/gabyroos_film/"></a>
                <a className="fa fa-facebook-square fa-3x" target="_blank" href="https://www.facebook.com/gabriellaroosfilm/"></a>
            </div>
        )
    }
}
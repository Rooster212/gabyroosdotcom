import React from 'react';

import { Link } from 'react-router-dom';

import videojs from 'video.js';

export default class Showreel extends React.Component<any, any> {

    videoNode: HTMLElement;
    player: any;
    poster = "/assets/WatchtowerOfBrighton_FirstFrame_BW.png";

    settings = {
        autoplay: true,
        controls: true,
        preload: 'auto',
        poster: this.poster,
        sources: [
            {
                src: '/assets/video/HomePageShowreelFull.mp4',
                type: 'video/mp4'
            }
        ]
    };

    componentDidMount()
    {
        this.player = videojs(this.videoNode, this.settings);
    }

    componentWillUnmount(){
        if(this.player) {
            this.player.dispose();
        }
    }

    render()
    {
        return (
            <div className="showreel-page">
                <Link to={"/"} className="back-button">
                    <span className={`fa fa-chevron-left`}></span> Back
                </Link>

                <video ref={node => this.videoNode = node} className="video-js vjs-big-play-centered" id="showreel"/>
            </div>
        );
    }
}
import React from 'react';

import Gallery from 'react-grid-gallery';
import axios from 'axios';

import { PhotographyProjectWithPhotos } from '../../types/photography';

export default class Photography extends React.Component<any, any> {
    constructor(props: any)
    {
        super(props);
        this.state = {
            galleries: []
        };
    }

    componentDidMount() {
        this.retrieveProjects();
    };

    setGalleryState = (projects: PhotographyProjectWithPhotos[]) => {
        this.setState({
            galleries: projects
        });
    }

    retrieveProjects = () => {
        axios.get('/photos/list/projects')
        .then((response: any) => {
            var photographyList = response.data as PhotographyProjectWithPhotos[];
            this.setGalleryState(photographyList);
        })
        .catch((error: any) => console.error(error));
    };

    renderGalleries = () => {
        if(this.state.galleries)
        {
            console.log("Galleries found");
            console.log(this.state.galleries);

            return this.state.galleries.map((v: PhotographyProjectWithPhotos, i: number) =>
                <div className="gallery">
                    <h3>{v.title}</h3>
                    <Gallery images={v.photos} key={i}
                        enableImageSelection={false}
                        lightboxWidth={1600}/>
                </div>
            );
        }
        else  {
            return (<h3>No galleries found.</h3>)
        }
    }

    render()
    {
        return (
            <div className="photography-page">
                <h2 className="align-center page-title">Photography</h2>
                {this.renderGalleries()}
            </div>
        );
    }
}
import React from 'react';

export default class Awards extends React.Component<any, any> {
    constructor(props: any)
    {
        super(props);
    }

    render()
    {
        const crown = "/assets/CrownPoster_CAMBSFILMFEST.png";
        const hybridMinds = "/assets/Drum&BassArenaAwards.jpeg";
        const hybridMindsThumbnail = "/assets/hybrid_minds_thumbnail.png";

        return (
            <div className="award-page">
                <h2 className="align-center page-title">Awards</h2>
                <div className="award-entry" id="crown-award-entry">
                    <h3>A Crown Fit For A Prince</h3>
                    <img id="crown-img" src={crown}></img>
                    <p>Gabriella worked with a very talented crew and took on the role of Editor and Colourist as well as a few other minor roles on A Crown Fit For a Prince.</p>
                    <p>Below are the awards and festival selections A Crown Fit For A Prince has received so far.
                        <ul>
                            <li>
                            WINNER - Award of Excellence - LA One Reeler Short Film Competition
                            </li>
                            <li>
                            WINNER - 1st Prize - Seaford Film Festival 2019
                            </li>
                            <li>
                            WINNER - Audience Award - Seaford Film Festival 2019
                            </li>
                            <li>
                            OFFICIAL SELECTION - Cambridge Film Festival 2018
                            </li>
                            <li>
                            OFFICIAL SELECTION - David Thomas Awards - Progress Film
                            </li>
                            <li>
                            SELECTED (waiting to hear) - BECTU Young Filmmakers Award
                            </li>
                        </ul>
                    </p>
                </div>
                <div className="award-entry" id="hybrid-minds-award-entry">
                    <h3>Hybrid Minds - Paint By Numbers (ft. Charlotte Haining)</h3>
                    <h4>Drum & Bass Arena Awards</h4>
                    <img id="hybrid-minds-img" src={hybridMinds}></img>
                    <p>While working on A Crown Fit For A Prince, Gaby also worked as Editor and Colourist, on a music video for highly acclaimed duo, Hybrid Minds. The video has had over 200K views on Youtube and came fourth in the ‘best music video’ category in the Drum & Bass Arena Awards. Considering their video was the only student production entered in to the competition, the crew were pretty chuffed to even be nominated, let alone come fourth behind the likes of groups such as Chase & Status.</p>
                    <img id="hybrid-minds-thumb" src={hybridMindsThumbnail}></img>
                </div>
            </div>
        );
    }
}
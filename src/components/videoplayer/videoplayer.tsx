import React from 'react';
import ReactPlayer, { Config as ReactPlayerConfig } from 'react-player'

import { VideoType } from '../../types/video';

export interface VideoPlayerProps {
    type: VideoType,
    url: string
}

export default class VideoPlayer extends React.Component<VideoPlayerProps, any> {
    constructor(props: VideoPlayerProps)
    {
        super(props);

        this.state = {
            type: props.type,
            url: props.url
        }
    }

    render()
    {
        return (
            <div className="video-container">
                <div className="react-player-wrapper">
                    <span className="video-close"></span>
                    {this.getVideo()}
                </div>
            </div>
        )
    }

    getVideo()
    {
        let stateType: VideoType = this.state.type;
        switch(stateType)
        {
            case "vimeo":
                return this.getVimeoVideo(this.state.url)
            case "youtube":
                return this.getYoutubeVideo(this.state.url)
        }
    }

    getYoutubeVideo(url: string)
    {
        const youtubeConfig: ReactPlayerConfig = {
            youtube: {
                playerVars: { rel: 0, iv_load_policy: 3, modestbranding: 1, controls: 1 }
            }
        };

        return (
            <ReactPlayer
                url={url}
                config={youtubeConfig}
                width='auto'
                height='auto'
            />
        );
    }

    getVimeoVideo(url: string)
    {
        const vimeoConfig: ReactPlayerConfig = {
            vimeo: {
                iframeParams: { title: true }
            }
        };

        return (
            <ReactPlayer
                url={url}
                config={vimeoConfig}
                width='auto'
                height='auto'
            />
        );
    }

}
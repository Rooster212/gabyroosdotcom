import React from 'react';

import Nav from './nav/nav';
import HomeBody from './homebody/homebody';
import About from './about/about';
import Films from './films/films';
import FilmPage from './films/filmpage';
import Contact from './contact/contact';
import Awards from './awards/awards';
import Testimonials from './testimonials/testimonials';
import Photography from './photography/photography';
import Showreel from './showreel';

import { BrowserRouter as Router, Route } from 'react-router-dom';

export class Home extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div className="body-container">
                <Router>
                    <div>
                        <Nav/>
                        <div className="main-body">
                            <Route exact path={"/"} component={HomeBody}/>
                            <Route path={"/about"} component={About}/>
                            <Route exact path={"/filmography"} component={Films}/>
                            <Route path={"/filmography/:id"} component={FilmPage} />
                            <Route path={"/photography"} component={Photography}/>
                            <Route path={"/awards"} component={Awards}/>
                            <Route path={"/testimonials"} component={Testimonials}/>
                            <Route path={"/contact"} component={Contact}/>
                            <Route path={"/showreel"} component={Showreel}/>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}
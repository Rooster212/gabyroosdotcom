import React from 'react';

import { Route, Link } from 'react-router-dom';

export default class Nav extends React.Component<any, any> {
    constructor(props: any)
    {
        super(props);
        this.state = {
            showMenuChecked: false
        };
    }

    onLinkClick = () =>
    {
        this.setState({showMenuChecked: false});
    }

    handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.checked;

        this.setState({
            showMenuChecked: value
        });
    }

    ActiveMenuLink = (props: {label: string, to: string, activeOnlyWhenExact?: boolean}) => (
        <Route path={props.to} exact={props.activeOnlyWhenExact || false} children={({ match }) => (
            <Link onClick={this.onLinkClick} className={match ? 'active' : ''} to={props.to}>{props.label}</Link>
        )}/>
      )

    render() {
        return (
            <div className="nav-bar-container disable-text-highlight">
                <Link onClick={this.onLinkClick} to="/">
                    <span className="logo">
                        <img src="/assets/logo3.png"></img>
                    </span>
                </Link>
                <input
                    checked={this.state.showMenuChecked}
                    onChange={this.handleCheckboxChange}
                    type="checkbox" id="show-menu" role="button"/>
                <label htmlFor="show-menu" className="show-menu">Menu<span className="fa fa-caret-down menu-icon"></span></label>
                <ul className="nav-bar">
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/" label="Home"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/about" label="About"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/filmography" label="Filmography"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/photography" label="Photography"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/awards" label="Awards"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/testimonials" label="testimonials"/>
                    </li>
                    <li>
                        <this.ActiveMenuLink activeOnlyWhenExact={true} to="/contact" label="Contact"/>
                    </li>
                </ul>
            </div>
        );
    }
}

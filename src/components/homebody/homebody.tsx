import React from 'react';
import Social from '../social/social';

export default class HomeBody extends React.Component<any, any> {
    render()
    {
        const poster = "/assets/WatchtowerOfBrighton_FirstFrame_BW.png";

        return (
            <div className="home-body">
                {/* <img src={poster} id="home-img"/> */}
                <video poster={poster} id="bgvid" playsInline autoPlay muted loop>
                    <source src="https://player.vimeo.com/external/354062579.hd.mp4?s=0c1e85d00d9705e34aeb47adcbc47b8f929960a5&profile_id=175" type="video/mp4"/>
                </video>

                <div className="home-overlay disable-text-highlight">
                    <h2 id="video-editor">Video Editor</h2>
                    <h2 id="location-text">Cambridge, UK</h2>
                    <div className="to-showreel-button-container">
                        <a target="_blank" href="https://vimeo.com/506494553" className="to-showreel-button">
                            <span className={`fa fa-play`}></span> Play Showreel
                        </a>
                    </div>
                </div>
                <Social/>
            </div>
        );
    }
}
import React from 'react';

export default class Testimonials extends React.Component<any, any> {

    render()
    {
        return (
            <div className="testimonials-page">
                <h2 className="align-center page-title">Testimonials</h2>
                <div className="testimonials-page-body">
                    <p>
                        "Gaby was the consummate professional, always prepared, adaptive to the changing needs of the location and shoot, working well with my clients, and bringing her own fun and creative personality to all aspects of the project. Her editing skills are exceptional and she has brought a new polish and professionally to the content on my website. I would highly recommend Gaby for any film and editing project, she will make you and your business look good!"
                        <br/>
                        <span>Tim Wildman, Director of WineTutor.tv</span>
                    </p>
                    <p>
                        “Gaby is exceptionally talented both technically and creatively. On top of this, she is super organised and 100% reliable. She was a stand out student in her year and I can’t recommend her highly enough. Also, very pleasant to be around with a great work ethic.”
                        <br/>
                        <span>Cath Pick (Brighton Film School’s Head of HND Filmmaking and Career & industry Liaison)</span>
                    </p>
                    <p>
                        “I have called upon Gaby’s post-production services on numerous occasions, and will continue to do so in the future. She has a vast technical knowledge and awareness, combined with an ability to offer advice from a creative perspective on any given project. I also highly recommend Gaby to colour correct and grade your footage.”
                        <br/>
                        <span>James Honess (Writer/Director)</span>
                    </p>
                    <p>
                        “For an emerging talent fresh out of film school, Gaby exudes the same passion, professionalism, and vision of someone who has spent years in the industry. With her meticulous attention to detail and colour, as well as a fresh creative perspective, she certainly has an exciting editing career ahead of her”
                        <br/>
                        <span>Sarah Morris (Producer)</span>
                    </p>
                    <p>
                        “Gaby’s passion and dedication for perfecting her craft and constantly looking for ways to improve are skills that will get her very far in the industry. Her fresh creative input, her commitment to every challenge that comes her way and her impressive technical knowledge make her a valuable addition to any project”
                        <br/>
                        <span>Brad James (Cinematographer)</span>
                    </p>
                </div>
            </div>
        );
    }
}
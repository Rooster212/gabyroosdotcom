import React from 'react';
import Social from '../social/social';

export default class Contact extends React.Component<any, any> {

    getEmail = () => atob('ZmlsbUBnYWJ5cm9vcy5jb20=');

    getEmailHref = () => 'mailto:' + this.getEmail();

    getPhoneNumber = () => atob('KCs0NCk3OTQwMTc4OTk1');

    getPhoneNumberHref = () => 'tel:' + this.getPhoneNumber();

    render()
    {
        return (
            <div className="contact-page">
                <h2 className="align-center page-title">Contact</h2>
                <div className="contact-page-body">
                    <div className="contact-page-logo">
                        <img src="/assets/logo3.png"></img>
                    </div>
                    <div className="ways-to-contact">
                        <div><span className="fa fa-envelope fa-3x"></span><a href={this.getEmailHref()}>{this.getEmail()}</a></div>
                        <div><span className="fa fa-phone fa-3x"></span><a href={this.getPhoneNumberHref()} >{this.getPhoneNumber()}</a></div>
                    </div>
                </div>
                <Social/>
            </div>
        );
    }
}
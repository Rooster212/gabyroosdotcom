import React from 'react';

import VideoPlayer from '../videoplayer/videoplayer';

import axios from 'axios';

import { VideoDefinition } from '../../types/video';

import { Link } from 'react-router-dom';

export default class FilmPage extends React.Component<any, any> {
    constructor(props: any)
    {
        super(props);

        var id = props.match.params.id;
        this.state = {
            videoId: id,
            videoDetails: null
        }
    }

    componentDidMount() {
        this.retrieveFilm();
    }

    retrieveFilm = () => {
        axios.get(`/videos/${this.state.videoId}`)
        .then((response: any) => {
            this.setState({
                videoDetails: response.data
            });
        })
        .catch((error: any) => console.error(error));
    }

    renderBody = () =>
    {
        if (this.state.videoDetails)
        {
            var video = this.state.videoDetails as VideoDefinition;

            return (
                <div id="filmpage-body">
                    <div className="filmpage-inner-body">
                        <div className="filmpage-info">
                            <Link to={"/filmography"} className="back-button desktop">
                                <span className={`fa fa-chevron-left`}></span> Back
                            </Link>

                            <h2>{video.title}</h2>
                            <p>{video.tagline}</p>
                            <p>{video.description}</p>
                        </div>
                        <VideoPlayer type={video.videoType} url={video.url}/>
                        <Link to={"/filmography"} className="back-button mobile">
                            <span className={`fa fa-chevron-left`}></span> Back
                        </Link>
                    </div>
                </div>
            )
        }
        else {
            return (<h4 className="align-center">Loading...</h4>)
        }
    }

    render()
    {
        return this.renderBody();
    }
}

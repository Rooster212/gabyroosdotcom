import React from 'react';

import axios from 'axios';

import { VideoDefinition } from '../../types/video';

import { Link } from 'react-router-dom';

export default class Films extends React.Component<any, any> {
    constructor(props: any)
    {
        super(props);
        this.state = {
            allVideos: [],
            videosToShow: []
        }
    }

    componentDidMount()
    {
        this.get_films();
    }

    get_films = () => {
        axios.get('/videos/list')
        .then((response: any) => {
            this.setState({
                allVideos: response.data.videos,
                videosToShow: response.data.videos
            });
        })
        .catch((error: any) => console.error(error));
    }

    render_gallery = () => {
        var videos = this.state.videosToShow as VideoDefinition[];
        if(videos.length > 0) {
            return videos.map((v, i) =>{
                var style = {
                    background: `url(${v.thumbnailUrl}) center no-repeat`,
                    backgroundSize: `cover`
                };

                return (
                    <Link to={`/filmography/${v.siteUrl}`}>
                        <div className="film-item" key={i} style={style}>
                            <div className="film-item-overlay">
                                <h4>{v.title}</h4>
                                <p>{v.tagline}</p>
                                {/* <p>{v.description}</p> */}
                            </div>
                        </div>
                    </Link>
                )
            });
        }
        else {
            return <h3>No videos to show</h3>
        }
    }

    render()
    {
        return (
            <div id="film-list">
                <h2 className="align-center page-title">Filmography</h2>
                <div className="films-container">
                    { this.render_gallery() }
                </div>
            </div>
        );
    }
}

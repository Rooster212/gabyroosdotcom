export interface VideoDefinition
{
    title: string,
    url: string,
    thumbnailUrl: string,
    videoType: VideoType,
    tagline: string?,
    description: string?,
    tags: string[]?,
    siteUrl: string
}

export type VideoType =
    "local"
    | "youtube"
    | "vimeo";


export interface Videos {
    videos: VideoDefinition[]
}

export interface VideoLoadError extends Videos {
    error: string,
    videos: VideoDefinition[]
}
export interface PhotographyProject {
    title: string,
    folderName?: string,
    tagline?: string,
    description?: string,
    tag?: string[],
}

export interface PhotographyProjectWithPhotos extends PhotographyProject{
    photos: ReactGridGalleryImageOptions[]
}

export interface ReactGridGalleryImageOptions {
    src: string,
    thumbnail: string,
    thumbnailWidth: number,
    thumbnailHeight: number,
    tags?: string[],
    isSelected?: boolean,
    caption?: string,
    srcset?: string[],
    customOverlay?: Element,
    thumbnailCaption?: string | Element
}

export interface Photography {
    photography: PhotographyProjectWithPhotos[]
}
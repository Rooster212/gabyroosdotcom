import { Router, Response, NextFunction } from 'express';
import { Request } from 'express-serve-static-core';

import * as path from 'path';
import * as fs from 'fs';

import { noCache, shuffleArray } from './helpers';

import {
    Photography,
    PhotographyProject,
    PhotographyProjectWithPhotos,
    ReactGridGalleryImageOptions
    } from '../types/photography';

const router = Router();
const photographyPath = path.resolve(__dirname + '../../../../assets/photography');

router.get('/folders', noCache, (_req, res, next) => {
    fs.readdir(photographyPath , (err, items) => {
        if(err) next(err);
        res.locals.folders = JSON.stringify(items);
        next();
    });
}, (_req, res) => { res.send(JSON.stringify(res.locals.folders)) });

export function getPhotoInfo(relativeFolderPath: string, absolutePath: string, name: string, thumbs: boolean = false) {
    var dimensions = {
        width: 100,
        height: 100
    };

    var relativePath = [relativeFolderPath, name].join('/');

    var thumbnailPath = '';
    if (thumbs)
    {
        var indexOfExt = name.lastIndexOf('.');
        var thumbName = name.slice(0, indexOfExt) + '_thumb.' + name.slice(indexOfExt+1);
        thumbnailPath = [relativeFolderPath, 'thumbs', thumbName].join('/');
    }

    try
    {
        var sizeOf = require('image-size');
        var d = sizeOf(absolutePath);
        dimensions.width = d.width / 7;
        dimensions.height = d.height / 7;
    }
    catch (err) {
        console.log(err);
    }

    var options: ReactGridGalleryImageOptions = {
        src: relativePath,
        thumbnail: thumbs ? thumbnailPath : relativePath,
        thumbnailHeight: dimensions.height,
        thumbnailWidth: dimensions.width
    };

    return options;
}

var getListOfProjects_middleware = (_req: Request, res: Response, next: NextFunction) => {
    var photographyJson = path.resolve(photographyPath, 'photography.json');
    fs.readFile(photographyJson, (err, data) => {
        if (err) next (err);

        if (data)
        {
            var jsonData = data.toString();
            var photographyData = JSON.parse(jsonData) as Photography;

            var photos = getPhotos(photographyData);

            res.locals.photographyData = JSON.stringify(photos);
        }
        else
        {
            res.locals.photographyData = [];
        }

        next();
    });
}

var getPhotos = (photographyData: Photography) => {
    return photographyData.photography.map(getPhotographyProject).filter(p => p != undefined);
}

var getPhotographyProject = (project: PhotographyProject) =>
{
    try
    {
        var photoProjectPath =  path.resolve(photographyPath, project.folderName);

        if(!fs.existsSync(photoProjectPath))
        {
            // if the folder doesn't exist, we are misconfigured
            // or the photos haven't been copied
            console.warn(`Path "${photoProjectPath}" wasn't found`);
            return undefined;
        }

        console.log(`Photo project ${project.title} folder: ${photoProjectPath}`);
        var folderContents = fs.readdirSync(photoProjectPath);

        var thumbnailsArePresent = false;
        if (folderContents.indexOf('thumbs')) {
            thumbnailsArePresent = true;
            folderContents = folderContents.filter(v => v !== 'thumbs');
        }

        var photos: ReactGridGalleryImageOptions[] = folderContents.map((v) => {
            var relativePath = ['/assets/photography', project.folderName].join('/');
            var photoAbsolutePath = [photoProjectPath, v].join('/');

            return getPhotoInfo(relativePath, photoAbsolutePath, v, thumbnailsArePresent);
        });

        var photosShuffled = shuffleArray(photos);

        var photographyProjectWithPhotos: PhotographyProjectWithPhotos = {
            title: project.title,
            description: project.description,
            folderName: project.folderName,
            tag: project.tag,
            tagline: project.tagline,
            photos: photosShuffled
        };
        return photographyProjectWithPhotos;
    }
    catch {
        // If we had an error, don't break!
        return undefined;
    }
}


router.get('/list/projects', noCache, getListOfProjects_middleware, (_req, res) => { res.send(res.locals.photographyData) });

router.get('/album/:album', noCache, (req, res, next) => {
    var albumName = req.params.album;
    var albumPath = path.resolve(photographyPath, albumName);
    fs.readdir(albumPath, (err, items) => {
        if (err) next(err);
        res.locals.photos = JSON.stringify(items);
        next();
    });
}, (_req, res) => { res.send(JSON.stringify(res.locals.photos))})

export default router;
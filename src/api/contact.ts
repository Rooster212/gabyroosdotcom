import { Router } from 'express';

const router = Router();

router.get('/phone', (_req, res) => {
    res.send("01234567890");
});

export default router;
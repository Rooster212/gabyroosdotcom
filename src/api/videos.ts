import { Router } from 'express';
import * as path from 'path';
import * as fs from 'fs';

import { noCache } from './helpers';
import { Videos } from '../types/video';

const router = Router();
const videosPath = path.resolve(__dirname + '../../../../assets/video');

router.get('/list', noCache, (_req, res, next) => {
    var videoJson = path.resolve(videosPath, 'videos.json');
    fs.readFile(videoJson, (err, data) => {
        if (err) next (err);
        res.locals.videoData = data.toString();
        next();
    });
}, (_req, res) => { res.send(res.locals.videoData) });

router.get('/:video', noCache, (req, res, next) => {
    var videoJson = path.resolve(videosPath, 'videos.json');
    fs.readFile(videoJson, (err, data) => {
        if (err) next (err);

        var params = req.params;
        var videoToGet = params.video as string;
        console.log(params);
        var videosAsString = data.toString();
        var videos = JSON.parse(videosAsString) as Videos;

        try
        {
            var video = videos.videos.filter(x => {
                return videoToGet.toLowerCase() === x.siteUrl.toLowerCase()
            })[0];

            res.locals.video = video;
        }
        catch
        {
            res.locals.video = {
                "error":"Error thrown attempting to retrieve film",
                "videos": videosAsString
            }
        }

        next();
    });
}, (_req, res) => { res.send(res.locals.video) });

export default router;
'use strict';
exports.run = function(useCompression) {
    var express = require('express');
    var path = require('path');
    var compression = require('compression');
    var contact = require('./src/js/api/contact').default;
    var photos = require('./src/js/api/photos').default;
    var video = require('./src/js/api/videos').default;

    var app = express();

    // Add compression
    if (useCompression) {
        app.use(compression());
    }

    // Setup routes
    app.use(express.static(__dirname + '/dist'));
    app.use('/assets', express.static(__dirname + '/assets'));

    // Setup APIs
    app.use('/contact', contact);
    app.use('/photos', photos);
    app.use('/videos', video);

    // Setup robots.txt and sitemap.xml
    app.get('/robots.txt', function (_request, response) {
        response.sendFile('/robots.txt', { root: path.resolve(__dirname) });
    });

    app.get('/sitemap.xml', function (_request, response) {
        response.sendFile('/sitemap.xml', { root: path.resolve(__dirname) });
    });

    // handle every other route with index.html, handled by React Router
    app.get('*', function (_request, response) {
        response.sendFile('index.html', { root: path.resolve(__dirname + '/dist') });
    });

    // Listen on port
    var port = process.env.PORT || 8080;
    app.listen(port);
};

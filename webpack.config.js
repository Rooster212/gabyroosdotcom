/* eslint-disable no-var, strict, prefer-arrow-callback */
'use strict';

var path = require('path');

var babelOptions = {
'presets': [
  'react',
  [
    'es2015',
    {
      'modules': false
    }
  ],
  'es2016'
]
};

module.exports = {
  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
  },
  cache: true,
  entry: {
    main: './src/index.tsx',
    vendor: [
      'babel-polyfill',
      'events',
      'fbemitter',
      'flux'
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist/scripts'),
    filename: '[name].js',
    chunkFilename: '[chunkhash].js'
  },
  module: {
    rules: [{
      test: /\.ts(x?)$/,
      exclude: [/node_modules/, path.resolve(__dirname, '/api/*')],
      use: [
        {
          loader: 'babel-loader',
          options: babelOptions
        },
        {
          loader: 'ts-loader'
        }
      ]
    }]
  },
  plugins: [
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  }
};

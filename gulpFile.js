/* eslint-disable no-var, strict, prefer-arrow-callback */
'use strict';

var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var eslint = require('gulp-eslint');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var ts = require('gulp-typescript');

var webpack = require('./gulp/webpack');
var staticFiles = require('./gulp/staticFiles');
var clean = require('./gulp/clean');
var inject = require('./gulp/inject');

var runner = require('./site_runner');

const lintSrcs = ['./gulp/**/*.js', './*.js'];
const sassSrcs = ['./src/styles/styles.scss'];
const tsSrcs   = ['./src/**/*.ts'];

const autoprefixerConfig = {
  browsers: ['last 4 version'],
  cascade: false
};

gulp.task('delete-dist', function (done) {
  clean.run(done);
});

var compileStyles = (compress, destination) =>
  gulp.src(sassSrcs)
    .pipe(sass({outputStyle: compress ? 'compressed' : 'nested'})
      .on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerConfig))
    .pipe(gulp.dest(destination));

gulp.task('sass-reload', function(done) {
  return compileStyles(false, './dist/styles');
});

gulp.task('sass', ['delete-dist'], function(done) {
  return compileStyles(false, './dist/styles');
});

gulp.task('sass-build', ['delete-dist'], function(done){
  return compileStyles(true, './dist/styles');
});

var tsProject = ts.createProject('tsconfig.json', {
  target: 'es6',
  module: 'commonjs'
});

gulp.task('compile-ts', function() {
  var tsResult = gulp.src(tsSrcs)
    .pipe(tsProject());

  return tsResult.js.pipe(gulp.dest('./src/js'));
});

gulp.task('compile-ts-build', ['delete-dist', 'compile-ts']);

gulp.task('build-js', ['delete-dist'], function(done) {
  webpack.build().then(function() { done(); });
});

gulp.task('build-other', ['delete-dist'], function() {
  staticFiles.build();
});

gulp.task('build', ['build-js', 'build-other', 'lint', 'sass-build', 'compile-ts-build'], function () {
  inject.build();
});

var lint = (fix) => {
  return gulp.src(lintSrcs)
    .pipe(eslint({
      configFile: './.eslintrc',
      fix: fix
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
};

gulp.task('lint', function () {
  return lint(false);
});

gulp.task('lint-and-fix', function() {
  return lint(true);
});

gulp.task('watch', ['delete-dist', 'sass', 'compile-ts-build'], function(done) {
  process.env.NODE_ENV = 'development';
  Promise.all([
    webpack.watch()
  ]).then(function() {
    gutil.log('Now that initial assets (js and css) are generated inject will start...');
    inject.watch();
    done();
  }).catch(function(error) {
    gutil.log('Problem generating initial assets (js and css)', error);
  });

  gulp.watch(lintSrcs, ['lint']);
  gulp.watch(sassSrcs, ['sass-reload']);
  gulp.watch(tsSrcs, ['compile-ts']);
  staticFiles.watch();
});

gulp.task('watch-and-serve', ['watch'], function() {
  runner.run(false);
});

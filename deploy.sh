#!/bin/sh

# this repo
LOGIN_COMMAND="docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY"
# NGINX repo
LOGIN_NGINX_COMMAND="docker login -u $CI_NGINX_DEPLOY_USER -p $CI_NGINX_DEPLOY_PASSWORD $CI_REGISTRY"
START_COMMAND="sshpass -p $DEPLOY_PASSWORD ssh -p $DEPLOY_PORT $DEPLOY_USERNAME@$DEPLOY_TARGET_SERVER"

DESTINATION_FILENAME="gabyr-docker-compose.yml"

function runCommandOverSSH {
    eval "$START_COMMAND $1"
}

function deploy {
    if runCommandOverSSH "$LOGIN_COMMAND"; then
    echo "Logged in to gabyroosdotcom repo"
    if runCommandOverSSH "$LOGIN_NGINX_COMMAND"; then
    echo "Logged in to NGINX repo repo"
    if runCommandOverSSH "docker-compose -f $DESTINATION_FILENAME pull"; then
    echo "Pulled latest containers"
    if runCommandOverSSH "docker-compose -f $DESTINATION_FILENAME up -d"; then
    echo "Started containers"
    if runCommandOverSSH "docker logout $CI_REGISTRY"; then
    echo "Logged out"
    fi; fi; fi; fi; fi;
}

# Copy docker-compose
sshpass -p $DEPLOY_PASSWORD scp -P $DEPLOY_PORT ./docker-compose.yml $DEPLOY_USERNAME@$DEPLOY_TARGET_SERVER:/home/$DEPLOY_USERNAME/$DESTINATION_FILENAME

deploy